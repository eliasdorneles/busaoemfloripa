#!/usr/bin/env python
# -*- encoding: utf-8

import re, sys
from bs4 import BeautifulSoup
from HTMLParser import HTMLParser

htmlParser = HTMLParser()

if len(sys.argv) < 2:
    print "Faltou argumento com caminho do arquivo"
    sys.exit(1)

page = open(sys.argv[1]).read() #.decode('latin1') #.replace(u'ã','&atilde;')
doc = BeautifulSoup(page)

url_strip = 'http://www.jotur.com.br/index.php?cmd=horario-detalhes&agrupador='
for tbl in doc('table'):
    #cidade = tbl('thead')[0]('tr')[0].th.string.encode('utf-8')
    #print 'CIDADE:', cidade
    for tr in tbl('tbody')[0]('tr'):
        anchor = tr.td.a
        if anchor:
            agrupador = anchor['href'].replace(url_strip, '')
            descr_linha = anchor.string.strip()
            descr_linha = descr_linha.replace('Fpolis', u'Florianópolis')
            descr_linha = re.sub(r' - R\$ [0-9,]+$', '', descr_linha)
            print '%s %s' % (agrupador.encode('utf-8'), descr_linha.encode('utf-8'))
