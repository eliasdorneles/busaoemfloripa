#!/usr/bin/env python
# -*- encoding: utf-8

import re, sys
from bs4 import BeautifulSoup
from HTMLParser import HTMLParser

htmlParser = HTMLParser()

if len(sys.argv) < 2:
    print "Faltou argumento com caminho do arquivo"
    sys.exit(1)

page = open(sys.argv[1]).read().decode('latin1').replace(u'ã','&atilde;')
doc = BeautifulSoup(page)

def imprime_linha(td_linha):
    linha = td_linha[0].div.strong if td_linha[0].div else td_linha[0].strong
    if not linha:
        return
    linha = unicode(linha.string)
    nome = td_linha[1].strong.a
    nome = unicode(nome.string)
    # remove espacos e html entities
    nome = htmlParser.unescape(re.sub(r'\s+', ' ', nome))
    print linha, nome.encode('utf-8')

for tr in doc('table')[1]('tr'):
    tds = tr('td')

    # primeiro grupo
    td_linha = tds[:2]
    # TODO: testar se eh valida
    imprime_linha(td_linha)

    td_linha = tds[2:]
    if not td_linha:
        break # entao ja foi a ultima linha
    imprime_linha(td_linha)
