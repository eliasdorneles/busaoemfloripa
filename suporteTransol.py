#!/usr/bin/python
# -*- encoding: utf-8

from bs4 import BeautifulSoup
from util import send_request
import re

class SuporteTransol:
    nomeEmpresa = 'Transol'
    siteEmpresa = 'http://www.transoltc.com.br'
    urlLinhas = siteEmpresa + '/principal.php'
    pageLinhas = ''
    def fetch_linhas_page(self):
        page = send_request(self.urlLinhas).read()
        #page = page.decode('latin1').encode('utf-8')
        return page
    def get_linhas_from_page(self, page):
        soup = BeautifulSoup(page)
        selectLinhas = soup.find('select', attrs={'name':'s_id_linha'})
        linhas = []
        for option in selectLinhas('option'):
            if option.has_key('value'):
                numeroLinha = option['value']
                # remove caractere tosco, se existir
                nomeLinha = re.sub(r'^(. )?', '', option.string)

                # remove numero da linha do fim:
                nomeLinha = re.sub(r' - [^ ]+$', '', nomeLinha)
                linhas.append([numeroLinha, nomeLinha])
        return linhas
    def busca_linhas(self):
        return self.get_linhas_from_page(self.fetch_linhas_page())

    def fetch_horario_page(self, id_linha):
        url = 'http://www.transoltc.com.br/secoes/horario_impressao.php'
        post_data = {'s_id_linha': id_linha, 's_periodo_escolhido': '1'}
        response = send_request(url, post_data).read()
        return response.decode('latin1').encode('utf-8')
    def _tabelas_com_horarios(self, soup):
        return [e.table
                for e in soup.table.table.tr.children
                if hasattr(e, 'name') and e.name == 'td']
    def _extrai_nomes_terminais(self, tabela):
        "dada uma table soup que contem os horarios, retorna os nomes dos terminais"
        terminais_saida = [' '.join(e.stripped_strings)
                for e in tabela.contents[3]
                if hasattr(e, 'name') and e.name == 'td']
        return [re.sub('Sa.da ', '', t) for t in terminais_saida]
    def _extrai_tipo_dia(self, tabela):
        assert hasattr(tabela, 'name')
        assert tabela.name == 'table'
        tipo = tabela.tr.td.font.string
        if re.match('(?i)domin?gos?', tipo):
            return 'domingos'
        if re.match('(?i)dias? .t(eis|il)', tipo):
            tipo = 'uteis'
        if re.match('(?i)s.bados?', tipo):
            tipo = 'sabados'
        return tipo
    def _extrai_texto_do_horario(self, td):
        assert hasattr(td, 'name')
        return ' '.join(list(td.strings)).strip()
    def _extrai_horarios(self, table):
        primeira_coluna = []
        segunda_coluna = []
        for tr in table.find_all('tr'):
            primeira_coluna.append(tr.find_all('td')[0])
            # coluna do meio eh soh o separador
            segunda_coluna.append(tr.find_all('td')[2])
        total = primeira_coluna + segunda_coluna
        for i, td in enumerate(total):
            # pega o conteudo da celula em texto:
            total[i] = self._extrai_texto_do_horario(td)
        # filtra celulas vazias
        total = filter(None, total)
        return total
    def busca_horarios_da_linha(self, id_linha):
        page = self.fetch_horario_page(id_linha)
        soup = BeautifulSoup(page)
        tables = self._tabelas_com_horarios(soup)
        horarios = {}
        for table in tables:
            terminais_saidas = self._extrai_nomes_terminais(table)
            tipo_dia = self._extrai_tipo_dia(table)
            tables_horarios = table.find_all('table')
            for i, saida in enumerate(terminais_saidas):
                if saida not in horarios:
                    horarios[saida] = {}
                horarios[saida][tipo_dia] = self._extrai_horarios(tables_horarios[i])
        return horarios

if __name__ == '__main__':
    m = SuporteTransol()
    print m.nomeEmpresa
    print m.siteEmpresa
    print m.busca_linhas()
