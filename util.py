import urllib2, urllib

def send_request(url, post_data={}):
    """Send an HTTP request and return response. If post_data is provided, use POST method"""
    req = urllib2.Request(url)
    if post_data:
        req.add_data(urllib.urlencode(post_data))
        # req.get_method() -> 'POST'
    return urllib2.urlopen(req)
