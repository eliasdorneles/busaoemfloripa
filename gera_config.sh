#!/bin/bash
# Uso: ./gera_config.sh update > config.py

update=false
[ "x$1" == xupdate ] && update=true
mkdir -p tmp

log(){
	echo "$*" >> tmp/gera_config.log
}
log "Iniciando gera_config.sh -- $(date)"

if $update; then
	# pega horários do site da Transol:
	log "Baixando horarios da Transol..."
	curl -s http://www.transoltc.com.br/principal.php | \
		iconv -f iso-8859-1 -t utf-8 | \
		grep '<option class="cinza_11"' > tmp/transol.txt && log "Feito" || log "Erro"
fi
transol=$(sed -E '
s/.*value="([^"]+)">. (.+)<\/option>/\1|\2/
s/ - [^ ]+$//  # remove o ID da linha do final
s/^([^|]+)\|(.*)/"\1": u"\2",/
' tmp/transol.txt)

if $update; then
	log "Baixando horarios municipais da Estrela..."
	curl -s http://www.tcestrela.com.br/ws1/linhas/entradas/municipal.htm > tmp/estrela-municipal.html && log "Feito" || log "Erro"
	log "Baixando horarios intermunicipais da Estrela..."
	curl -s http://www.tcestrela.com.br/ws1/linhas/entradas/intermunicipal.htm > tmp/estrela-intermunicipal.html && log "Feito" || log "Erro"
	log "Baixando horarios interbairros da Estrela..."
	curl -s http://www.tcestrela.com.br/ws1/linhas/entradas/interbairros.htm > tmp/estrela-interbairros.html && log "Feito" || log "Erro"
fi
estrela_municipal=$(python extrai-estrela.py tmp/estrela-municipal.html | sed -E 's/^([0-9]+) (.*)$/"municipal\/\1": u"\2",/')
estrela_intermunicipal=$(python extrai-estrela.py tmp/estrela-intermunicipal.html | sed -E 's/^([0-9]+) (.*)$/"intermunicipal\/\1": u"\2",/')
estrela_interbairros=$(python extrai-estrela.py tmp/estrela-interbairros.html | sed -E 's/^([0-9]+) (.*)$/"interbairros\/\1": u"\2",/')
estrela="$estrela_municipal
$estrela_intermunicipal
$estrela_interbairros"

if $update; then
	log "Baixando horarios da Canasvieiras..."
	wget --quiet http://www.canasvieirastc.com.br/pt/horarios -O tmp/canasvieiras.html && log "Feito" || log "Erro"
fi
canasvieiras=$(python extrai-canasvieiras.py tmp/canasvieiras.html | sed -E 's/^([^ ]+) (.*)/"\1": u"\2",/')

if $update; then
	curl -s "http://www.jotur.com.br/index.php?cmd=horarios" > tmp/jotur.html && log "Feito" || log "Erro"
fi
jotur=$(python extrai-jotur.py tmp/jotur.html | sed -E 's/^([^ ]+) (.*)/"\1": u"\2",/')

echo "#!/usr/bin/env python"
echo "# -*- encoding: utf-8"
echo
echo "LINHAS = {"
echo '    "Transol": {'
echo "$transol" | sed 's/^/        /'
echo "    },"
echo '    "Estrela": {'
echo "$estrela" | sed 's/^/        /'
echo "    },"
echo '    "Canasvieiras": {'
echo "$canasvieiras" | sed 's/^/        /'
echo "    },"
echo '    "Jotur": {'
echo "$jotur" | sed 's/^/        /'
echo "    },"
echo "}"
