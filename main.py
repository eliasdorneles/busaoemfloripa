#!/usr/bin/env python
# -*- encoding: utf-8

import webapp2, jinja2, os, re
from google.appengine.ext import db
from google.appengine.api import memcache
from datetime import datetime, timedelta
import logging
from config import LINHAS
from bs4 import BeautifulSoup
from util import send_request
from suporteTransol import SuporteTransol
from horarios import Empresa, LinhaOnibus, Terminal, Horario

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_loader = jinja2.FileSystemLoader(template_dir)
jinja_env = jinja2.Environment(loader=jinja_loader, autoescape=True)

# regex replace for Jinja
jinja_env.filters['re_replace'] = lambda s, regex, replace: re.sub(regex, replace, s)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)
    def writeln(self, text):
        "metodo tosco de escrever 'linhas'"
        self.write(text + '<br />\n')
    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    # helper for json
    def write_json(self, *a, **kw):
        self.response.headers['Content-type'] = 'application/json; charset=UTF-8'
        self.response.out.write(*a, **kw)

    # helpers for cookies
    def set_cookie(self, cookie):
        self.response.headers.add_header('Set-Cookie', cookie)
    def delete_cookie(self, cookie):
        self.set_cookie(cookie + '; Expires=Thu, 01 Jan 1970 00:00:01 GMT')
    def get_cookie(self, cookie):
        if cookie in self.request.cookies:
            return self.request.cookies[cookie]

LOG = lambda x: logging.error(x)

def get_horario_estrela(linha, empresa=None):
    url = 'http://www.tcestrela.com.br/ws1/linhas/horarios/' + linha + ".htm"
    response = send_request(url).read()
    return response.decode('latin1').encode('utf-8')

def get_horario_canasvieiras(linha, empresa=None):
    url = 'http://www.canasvieirastc.com.br/pt/horarios/imprimir-sem-mapa/' + linha
    response = send_request(url).read()
    return response # response already is utf-8

def limpa_jotur(page):
    doc = BeautifulSoup(page)
    result = doc.find_all('div', class_='corpo-meio')[0]

    # remove alguns elementos desnecessarios
    # XXX: tá na hora de começar a parsear isso direito e meter no datastore!
    # Tá dando trampo demais pra resultados muito mazomeno, usando o HTML dos sites
    for elem in result.find_all('input') + result.find_all('div', class_='form_test'):
        elem.decompose()
    for elem in result.find_all('a'):
        elem.decompose()
    for elem in result.find_all('div', class_='clear'):
        elem.decompose()
    result('h2')[0].decompose()

    return result

def get_horario_jotur(linha, empresa=None):
    url = 'http://www.jotur.com.br/index.php?cmd=horario-detalhes&agrupador=' + linha
    response = send_request(url).read()
    return limpa_jotur(response).encode('utf-8')

def get_horario_from_datastore(linha, empresa=None):
    empresaStored = Empresa.get(db.Key.from_path('Empresa', empresa))
    linhaStored = LinhaOnibus.get(db.Key.from_path('LinhaOnibus', linha))
    LOG(empresaStored)
    LOG(linhaStored)
    #terminais = db.GqlQuery("SELECT * FROM Terminal WHERE empresa = :1 AND linha = :2", empresaStored.key(), linhaStored.key())
    params = {
            'empresa': empresaStored,
            'linha': linhaStored,
            #'terminais': terminais
            }
    t = jinja_env.get_template('horario.html', params)
    return t.render()

def get_horario_da_web(linha):
    # TODO: fazer isso mais robusto
    linha, empresa = linha.split('@')
    return {
            #"Transol": get_horario_from_datastore,
            "Transol": lambda x, y: SuporteTransol().fetch_horario_page(x),
            "Estrela": get_horario_estrela,
            "Canasvieiras": get_horario_canasvieiras,
            "Jotur": get_horario_jotur,
            }[empresa](linha, empresa)

def get_horario(linha, update=False):
    key = linha
    data = memcache.get(key)
    if data is not None and not update:
        ts, horarios = data
    else:
        horarios = get_horario_da_web(linha)
        try:
            horarios = get_horario_da_web(linha)
        except:
            # caso der problema ao pegar da web, pega do memcache se existir
            ten_days_ago = datetime.today() - timedelta(10)
            return data if data is not None else (ten_days_ago.strftime('%s'), u'Não disponível')

        ts = datetime.now().strftime('%s')
        memcache.set(key, (ts, horarios))
    return ts, horarios

class MainHandler(Handler):
    def get(self):
        tuplas_linhas = []
        for empresa, linhas in LINHAS.iteritems():
            for linha, nome_linha in linhas.iteritems():
                tupla = (empresa, linha, nome_linha)
                tuplas_linhas.append(tupla)
        tuplas_linhas.sort(key=lambda x: x[2])
        self.render('index.html', linhas=tuplas_linhas)
class HorarioHandler(Handler):
    def post(self):
        ts, horarios = get_horario(self.request.get('linha'))
        seconds_ago = int(datetime.now().strftime('%s')) - int(ts)

        # caso pagina no memcache existe há mais de 7 dias, tenta buscar de novo
        if seconds_ago > 7 * 3600:
            ts, horarios = get_horario(self.request.get('linha'), update=True)

        self.write(horarios)

def check_secret(secret):
    # (in)segurança por obscuridade... :P
    # TODO: botar o hash aqui, pra não ficar tão descarado
    return secret == 'botemo123enonfroxemo'

def cria_ou_atualiza_horarios_da_linha(linha, suporte):
    horarios = suporte.busca_horarios_da_linha(linha.id_externo)
    for nome_terminal in horarios:
        terminal = Terminal.get_or_insert(key_name=nome_terminal,
                nome=nome_terminal)

        # recria horarios da linha
        db.delete(Horario.all(keys_only=True).ancestor(linha))
        for tipo_dia in horarios[nome_terminal]:
            for hora in horarios[nome_terminal][tipo_dia]:
                Horario(parent=linha, hora=hora, linha=linha,
                        tipo_dia=tipo_dia, origem=terminal).put()

def key_linha(id_externo, empresa):
    return id_externo + '@' + empresa.nome

def cria_ou_atualiza_linhas(empresa, suporte):
    linhas = suporte.busca_linhas()
    li = []
    for l in linhas:
        id_externo, nome_linha = l
        key_name = key_linha(id_externo, empresa)
        li.append(LinhaOnibus.get_or_insert(key_name=key_name, empresa=empresa,
                nome=nome_linha, id_externo=id_externo))
    return li

def cria_ou_atualiza_empresa(nome, e):
    return Empresa.get_or_insert(key_name=nome, nome=nome,
            url_nome=e['url_nome'], site=e['site'])

EMPRESAS = {
        'Transol': {
            'url_nome': 'transol',
            'site': 'http://www.transoltc.com.br',
            'classe_suporte': SuporteTransol,
            },
        }
class ResetHandler(Handler):
    def get(self):
        self.render('reset.html')
    def post(self):
        secret = self.request.get('secret')
        if check_secret(secret):
            for nome in EMPRESAS:
                e = EMPRESAS[nome]
                empresa = cria_ou_atualiza_empresa(nome, e)
                LOG('empresa criada:' + empresa.nome)

                suporte = e['classe_suporte']()
                LOG('Atualizando linhas da empresa %s...' % nome)
                linhas = cria_ou_atualiza_linhas(empresa, suporte)
                LOG('Feito!')

                for li in linhas:
                    cria_ou_atualiza_horarios_da_linha(li, suporte)
                    LOG('Atualizado horarios da linha %s...' % li.nome)
            LOG('Feito!')
        else:
            self.redirect('/')
class ReportHandler(Handler):
    def get(self):
        for e in Empresa.all():
            self.writeln('Linhas da empresa ' + e.nome)

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/horario', HorarioHandler),
    ('/reset', ResetHandler),
    ('/report', ReportHandler),
    ], debug=True)
