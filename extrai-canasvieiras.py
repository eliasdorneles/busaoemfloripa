#!/usr/bin/env python
# -*- encoding: utf-8

import sys, re
from bs4 import BeautifulSoup

if len(sys.argv) < 2:
    print "Faltou argumento com caminho do arquivo"
    sys.exit(1)

page = open(sys.argv[1]).read()
doc = BeautifulSoup(page)
section = doc('section', {'id': 'pagina'})[0]
for div in section('div')[0]('div'):
    for li in div('ul')[0]('li'):
        id_linha = unicode(li.a['href'].replace('/pt/horarios/','').replace('.html', ''))
        nome = unicode(re.sub('^(D )?[0-9]+ ', '', li.a.string))
        #print '%s|%s' % (id_linha, nome)
        print id_linha, nome.encode('utf-8')
