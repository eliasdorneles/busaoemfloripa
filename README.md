Como rodar localmente
=====================

1. Baixe e instale a Google App Engine para Python aqui: <https://developers.google.com/appengine/downloads>
2. Clone o repo:

  * git clone https://eljunior@bitbucket.org/eljunior/busaoemfloripa.git

2. Rode o projeto usando o simulador da GAE

  * no Linux: na linha de comando `dev_appserver.py DIRETORIO_PROJETO`
  * no Windows: não sei, descubra e me conte!

3. Se tudo deu certo, abra o navegador em <http://localhost:8080> e seja feliz

