#!/usr/bin/env python
# -*- encoding: utf-8

from google.appengine.ext import db

class Empresa(db.Model):
    nome = db.StringProperty(required=True)
    url_nome = db.StringProperty(required=True)
    site = db.StringProperty(required=True)

class LinhaOnibus(db.Model):
    nome = db.StringProperty(required=True)
    url_nome = db.StringProperty()
    id_externo = db.StringProperty(required=True)
    extra = db.StringProperty()
    empresa =  db.ReferenceProperty(Empresa)

class Terminal(db.Model):
    nome = db.StringProperty(required=True)

class Horario(db.Model):
    hora = db.StringProperty(required=True)
    tipo_dia = db.StringProperty(required=True, choices=set([ 'uteis', 'sabados', 'domingos' ]))
    extra = db.StringProperty()
    linha =  db.ReferenceProperty(LinhaOnibus)
    origem = db.ReferenceProperty(Terminal)

